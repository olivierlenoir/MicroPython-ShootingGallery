"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2022-07-11 20:44:06
License: MIT, Copyright (c) 2022 Olivier Lenoir
Language: MicroPython v1.19.1
Project: Shooting Gallery
Description:
Repository: https://gitlab.com/olivierlenoir/MicroPython-ShootingGallery
"""

from random import choice
from machine import Pin, PWM
from utime import sleep_ms


class Target:
    """Servo target"""

    _id = 0

    def __init__(self, servo_pin, hide_duty_ns, show_duty_ns):
        self._target_id = Target._id
        Target._id += 1
        self.servo = PWM(Pin(servo_pin))
        self.hide_duty_ns = hide_duty_ns
        self.show_duty_ns = show_duty_ns
        self.init()

    def init(self):
        """Init target"""
        self.servo.freq(50)
        self.servo.duty_ns(self.hide_duty_ns)

    def target_id(self):
        """Return target id"""
        return self._target_id

    def hide(self):
        """Hide target"""
        self.servo.duty_ns(self.hide_duty_ns)

    def show(self):
        """Show target"""
        self.servo.duty_ns(self.show_duty_ns)


class ShootingGallery:
    """Shooting gallery, made of target object"""
    def __init__(self):
        self.targets = []
        self.show_ms = 3000
        self.hide_ms = 500
        self.target = None
        self.target_multi = set()

    def add(self, target_obj):
        """Add target"""
        self.targets.append(target_obj)

    @property
    def shot_time(self):
        """Return or set shot time in ms"""
        return self.show_ms

    @shot_time.setter
    def shot_time(self, duration_ms):
        self.show_ms = duration_ms

    @property
    def hide_time(self):
        """Return or set hide time in ms"""
        return self.hide_ms

    @hide_time.setter
    def hide_time(self, duration_ms):
        self.hide_ms = duration_ms

    def choice(self):
        """Choice a target"""
        self.target = choice(self.targets)

    def choice_multi(self, multi):
        """Choice up to multi target"""
        self.target_multi = set()
        for _ in range(multi):
            self.target_multi.add(choice(self.targets))

    def hide_choice(self):
        """Hide choice target"""
        self.target.hide()

    def show_choice(self):
        """Show choice target"""
        self.target.show()

    def hide_choice_multi(self):
        """Hide choice multi targets"""
        for target in self.target_multi:
            target.hide()

    def show_choice_multi(self):
        """Show choice multi targets"""
        for target in self.target_multi:
            target.show()

    def hide_all(self):
        """Hide all targets"""
        for target in self.targets:
            target.hide()

    def show_all(self):
        """Show all targets"""
        for target in self.targets:
            target.show()

    def shuffle(self):
        """Shuffle target generator"""
        targets_id = list(range(len(self.targets)))
        for _ in targets_id:
            targets_id.append(targets_id.pop(choice(targets_id)))
        for target in targets_id:
            yield self.targets[target]

    def rand(self, n_shot=10):
        """Choice n time target"""
        self.hide_all()
        for shot in range(n_shot):
            print(f'Shot number: {shot+1}/{n_shot}')
            self.choice()
            self.show_choice()
            sleep_ms(self.show_ms)
            self.hide_choice()
            sleep_ms(self.hide_ms)

    def rand_multi(self, n_shot=10, multi=3):
        """Choice n time up to multi targets"""
        self.hide_all()
        for shot in range(n_shot):
            print(f'Shot number: {shot+1}/{n_shot}')
            self.choice_multi(multi)
            self.show_choice_multi()
            sleep_ms(self.show_ms)
            self.hide_choice_multi()
            sleep_ms(self.hide_ms)

    def all_once(self):
        """Shoot all targets once"""
        self.hide_all()
        for target in self.shuffle():
            target.show()
            sleep_ms(self.show_ms)
            target.hide()
            sleep_ms(self.hide_ms)
