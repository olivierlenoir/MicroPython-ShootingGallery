"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2022-07-11 21:00:51
License: MIT, Copyright (c) 2022 Olivier Lenoir
Language: MicroPython v1.19.1
Project:
Description:
Repository:
"""


from shootinggallery import Target, ShootingGallery

# Define targets
target_1 = Target(2, 100_000, 600_000)
target_2 = Target(4, 100_000, 600_000)
target_3 = Target(16, 100_000, 600_000)
target_4 = Target(17, 100_000, 600_000)
target_5 = Target(5, 100_000, 600_000)
target_6 = Target(18, 100_000, 600_000)

# Create shooting gallery
shooting_gallery = ShootingGallery()
shooting_gallery.add(target_1)
shooting_gallery.add(target_2)
shooting_gallery.add(target_3)
shooting_gallery.add(target_4)
shooting_gallery.add(target_5)
shooting_gallery.add(target_6)

shooting_gallery.hide_all()

# Shoot random target 15 times
shooting_gallery.rand(15)

# Shoot random up to multi targets 15 times
shooting_gallery.rand_multi(15, 3)

# Shoot each target once
shooting_gallery.all_once()
